<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Enterprise;
use Log;

class AdminController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard(Request $request)
    {
        if($request->user()->authorizeRoles(['Application Administrator'])) {

        	 return view('admin.dashboard');
        }
        else {
        	return redirect()->guest(route('hello'))->with('warning','noaccess');

        }
        
    }

    public function users(Request $request)
    {
        $users =  User::all();

        return view('admin.users', compact('users'));
    }

    public function enterprises(Request $request){
        $enterprises = Enterprise::all();

        return view('admin.enterprises', compact('enterprises'));
    }


    /**
     * Handle a creation of a user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function  createuser(Request $request)
    {
        $validator = $this->validator($request->all());
 
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
 
        $this->create($request->all());
 
        return response()->json();
    }

    public function storeuser(Request $request)
    {
        $data = $request->password ? $request->all():$request->except('password');
        
        if(!$request['changeEmailCheckbox']){
                array_forget($data, 'email');
        
        }

        $validator = $this->validator($data);
 
        if ($validator->fails()) {
            log::info('validation failed');
            $this->throwValidationException(
                $request, $validator
            );
        } else {
            // store
            log::info('request id:' . $request['id']);
            $user = User::findOrFail($request['id']);
            $user->update($data);
            if(array_has($data, 'password')) {
                $user->password =  bcrypt($data['password']);
            }

            $user->save();
        }
        return response()->json();

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'first_name'=> $data['first_name'],
            'last_name' => $data['last_name'],
            'company_name' => $data['company_name'],
            'vatnumber' => $data['vatnumber'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);


        //For now registration of user make them administrator
        $user
           ->roles()
           ->attach(Role::where('name', 'Administrator')->first());

        return $user;
    }
       
}
