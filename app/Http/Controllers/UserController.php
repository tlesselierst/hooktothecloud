<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Auth;
use App\User;
use Log;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('registered.dashboard');
    }

      public function profile()
    {
        $user = Auth::user();
        return view('registered.profile',compact('user'));    
    }

    public function updateProfile(Request $request)
    {
        $data = $request->password ? $request->all():$request->except('password');
        
        if(!$request['changeEmailCheckbox']){
                array_forget($data, 'email');
        
        }

        $validator = $this->validator($data);
 
        if ($validator->fails()) {
            log::info('validation failed');
            $this->throwValidationException(
                $request, $validator
            );
            //return Redirect::back()->with('error',trans('messages.profileupdate_failed'));

        } else {
            $authuser = Auth::user();
            if ($authuser['id']== $request['id']) {
                $user = User::findOrFail($request['id']);
                $user->update($data);
                if(array_has($data, 'password')) {
                    $user->password =  bcrypt($data['password']);
                }
                 $user->save();
                return Redirect::back()->with('success',trans('messages.profileupdate_done'));

           }  else {
                log::info("Tried to update other profile");
                return Redirect::back()->with('error',trans('messages.profileupdate_failed_wrong_profile'));

           }
            } 
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'first_name' => 'required|min:3',
            'last_name' => 'required|min:3',
        ]);
    }

}
