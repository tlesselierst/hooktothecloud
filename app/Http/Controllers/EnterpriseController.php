<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Enterprise;
use Auth;
use Illuminate\Support\Facades\Validator;
use Log;

class EnterpriseController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     **/
    public function __construct()
    {
        $this->middleware('auth');
    }

        public function listMyEnterprises(Request $request){
        	$user = Auth::user();
        	$enterprises = $user->enterprises;

            return view('registered.myenterprises', compact('enterprises'));

        }

    /**
     * Handle a creation of an enterprise 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     **/
    public function  createEnterprise(Request $request)
    {
        $data =  $request->all();       
        if(!$request['VATcheckbox']){
         array_forget($data, 'vatnumber');
        }

        $validator = $this->validator($data);
 
        if ($validator->fails()) {
             log::info('validation failed');
            $this->throwValidationException(
                $request, $validator
            );
        }
 
        $this->create($data);
 
        return response()->json();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     **/
    protected function create(array $data)
    {
      
        if(array_has($data, 'vatnumber')) {
          $enterprise = Enterprise::create([
            'enterprise_name'=> $data['enterprise_name'],
            'vatnumber' => $data['vatnumber'],
            'cbo' => $data['cbo']
            ]);
        }   else {
             $enterprise = Enterprise::create([
            'enterprise_name'=> $data['enterprise_name'],
            'cbo' => $data['cbo']
            ]);
        }

        //For now registration of user make them administrator
        $enterprise
           ->users()
           ->attach(Auth::user());

        return $enterprise;

    }

        /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'enterprise_name' => 'required|string|max:255',
            'cbo' => 'required|unique:enterprises|numeric|min:9',
        ]);
    }
}
