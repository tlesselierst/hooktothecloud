<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'enterprise_name', 'vatnumber', 'cbo','invoice_address_id'
        ];

    public function users()
	{
	  return $this
	    ->belongsToMany('App\User');
	}
}
