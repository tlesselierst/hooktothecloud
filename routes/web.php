<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group([
'prefix' => LaravelLocalization::setLocale(),
'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function()
{
	/** ADD ALL LOCALIZED ROUTES INSIDE THIS GROUP **/
	Route::get('/', function()
	{
		return View::make('public.hello');
	});

	Route::get('hello','HomeController@hello')->name('hello');

	Auth::routes();

	Route::get('dashboard', 'UserController@dashboard')->name('dashboard');
	Route::get('profile', 'UserController@profile')->name('profile');
	Route::post('updateProfile', 'UserController@updateProfile')->name('updateProfile');

	Route::get('myenterprises', 'EnterpriseController@listMyEnterprises')->name('myenterprises');
	Route::post('createnterprise', 'EnterpriseController@createEnterprise')->name('enterprise.create');

	Route::get('administration', 'AdminController@dashboard')->name('administration');
	

	Route::get('users', 'AdminController@users')->name('users');
	Route::get('enterprises', 'AdminController@enterprises')->name('enterprises');
	
	Route::post('storeuser', 'AdminController@storeuser')->name('user.store');
	Route::post('createuser', 'AdminController@createuser')->name('user.create');
	
});

/** OTHER PAGES THAT SHOULD NOT BE LOCALIZED **/



