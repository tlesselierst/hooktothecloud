let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');


// copying css from resources to public
mix.copy('resources/assets/css/bootstrap.min.css', 'public/css/bootstrap.min.css')
mix.copy('resources/assets/css/mdb.min.css','public/css/mdb.min.css')
mix.copy('resources/assets/css/httc.css', 'public/css/httc.css')

//copying js file to public
mix.copy('resources/assets/js/jquery-3.1.1.min.js','public/js/jquery-3.1.1.min.js')
mix.copy('resources/assets/js/bootstrap.min.js','public/js/bootstrap.min.js')
mix.copy('resources/assets/js/mdb.min.js', 'public/js/mdb.min.js')
mix.copy('resources/assets/js/tether.min.js','public/js/tether.min.js')
mix.copy('node_modules/popper.js/dist/popper.js', 'public/js/popper.js')

//fonts 
mix.copy('node_modules/font-awesome/css/font-awesome.min.css', 'public/css/font-awesome.min.css')
mix.copy('node_modules/font-awesome/fonts', 'public/fonts')
mix.copy('resources/assets/font/roboto', 'public/font/roboto')
mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap','public/fonts');

//images & co 
mix.copy('resources/assets/img', 'public/img')
mix.copy('resources/assets/vid', 'public/vid');