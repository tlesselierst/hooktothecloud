<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
	public function run()
	  {
	    $role_employee = new Role();
	    $role_employee->name = 'Application Administrator';
	    $role_employee->description = 'This role gives access to application administration screens';
	    $role_employee->assignable = false;
	    $role_employee->save();

	    $role_manager = new Role();
	    $role_manager->name = 'Administrator';
	    $role_manager->description = 'Administrator of a client';
	    $role_manager->assignable =true;
	    $role_manager->save();

	    $role_manager = new Role();
	    $role_manager->name = 'User';
	    $role_manager->description = 'User of a client';
	    $role_manager->assignable = true;
	    $role_manager->save();
	  }

}
