<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$role_manager  = Role::where('name', 'Application Administrator')->first();

    	$employee = new User();
    	$employee->name = 'Tanguy Lesseliers';
    	$employee->first_name = 'Tanguy';
    	$employee->last_name ='Lesseliers';
    	$employee->company_name = 'Home';
    	$employee->email = 'tanguy@hooktothecloud.com';
    	$employee->password = bcrypt('ikkeikke');
    
    	$employee->save();
    
    	$employee->roles()->attach($role_manager);
    }
}
