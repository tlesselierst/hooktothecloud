<?php

return array (
  'edit' => 
  array (
    'button' => 
    array (
      'save' => 'Save',
    ),
    'modaltitle' => 'Edit the user',
    'title' => 'Change the user details in this form',
    'changeEmail' => 'Change Email',
    'leaveemptypassword' => 'If you want to change the password, please enter the values. If you don\'t want to change anything, leave it empty',
  ),
  'add' => 
  array (
    'modaltitle' => 'Add a new user',
    'button' => 
    array (
      'create' => 'Create',
      'register' => 'Register',
    ),
    'label' => 
    array (
      'company' => 'Company',
      'confirmpwd' => 'Confirm Password',
      'displayname' => 'Display name',
      'email' => 'Email',
      'firstname' => 'Firstname',
      'lastname' => 'Lastname',
      'password' => 'Password',
      'vatnumber' => 'Vat number',
    ),
    'title' => 'Fill out all details. The user will receive an email',
  ),
  'header' => 
  array (
    'firstname' => 'Firstname',
    'lastname' => 'Lastname',
    'name' => 'Display Name',
    'company_name' => 'Company',
    'email' => 'Email',
    'action' => 'Action',
  ),
  'btn' => 
  array (
    'edit' => 'Edit',
    'addnewuser' => 'New User',
  ),
  'ecit' => 
  array (
    'changeEmail' => 'Change Email',
  ),
  'leaveemptypassword' => 'If you want to change the password, please enter the values. If you don\'t want to change anything, leave it empty',
);
