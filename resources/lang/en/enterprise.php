<?php

return array (
  'add' => 
  array (
    'modaltitle' => 'Add a new enterprise',
    'button' => 
    array (
      'create' => 'Create',
    ),
    'label' => 
    array (
      'cbo' => 'CBO number',
      'enterprise_name' => 'Enterprise name',
      'vatnumber' => 'VAT number',
    ),
    'title' => 'Add a new enteprise to your account',
    'tooltip' => 
    array (
      'vatnumber' => 'You need to check VAT? if you want to add it. Uncheck it if you don\'t have a VAT number',
    ),
  ),
  'edit' => 
  array (
    'VATpresent' => 'VAT ?',
  ),
);
