<?php

return array (
  'slide3' => 
  array (
    'btn' => 
    array (
      'action' => 'Access our support',
    ),
    'oneliner' => 'Use the community to get your answerts',
    'title' => 'Support @ HoohktotheCloud',
  ),
  'next' => 'next',
  'previous' => 'previous',
  'slide1' => 
  array (
    'btn' => 
    array (
      'login' => 'Login',
      'signup' => 'Sign Up',
    ),
    'oneliner' => 'Link your existing infrastructure to the cloud',
    'title' => 'HooktotheCloud',
  ),
  'slide2' => 
  array (
    'btn' => 
    array (
      'startconfig' => 'Start your first route',
    ),
    'oneliner' => 'And exchange documents in minutes',
    'title' => 'Kickstart with HooktotheCloud',
  ),
);
