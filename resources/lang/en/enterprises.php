<?php

return array (
  'header' => 
  array (
    'enterprise_name' => 'Enterprise',
    'vatnumber' => 'VAT number',
    'cbo' => 'CBO Number',
    'invoice_address_id' => 'Primary Invoice address',
    'action' => 'Action',
  ),
  'btn' => 
  array (
    'addNewEnterprise' => 'New Enterprise',
    'edit' => 'Edit',
  ),
  'title' => 
  array (
    'addNewEnterprise' => 'Add another entperprise',
  ),
);
