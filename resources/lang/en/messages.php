<?php

return array (
  'enterprise_created' => 'The enterprise has been created',
  'error_creating_enterprise' => 'The enterprise could not be added. Please see the creation form to see why.',
  'profileupdate_failed' => 'Your profile could not be updated',
  'profileupdate_done' => 'You updated your profile',
  'profileupdate_failed_wrong_profile' => 'There is a mismatch in profile. Update was canceled.',
  'logged_in' => 'You are logged in now.',
  'logged_out' => 'Thank you. You\'re logged out now.',
  'registered' => 'Great! You\'re registered now. Please log in.',
  'error_registered' => 'There were some issues when trying to register. Please check the registration form.',
  'noaccess' => 'The page you want to access is not accessible for you.',
  'notloggedin' => 'You\'re trying to access a secured page. Please login first',
  'user_created' => 'The user was created. He will receive an email.',
  'error_creating_user' => 'The user could not be created. Please check the form',
  'user_updated' => 'User details have been updated',
  'error_updating_user' => 'User details could not be updated',
);
