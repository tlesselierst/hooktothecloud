<?php

return array (
  'add' => 
  array (
    'label' => 
    array (
      'street' => 'Street',
      'zip' => 'Zip',
      'city' => 'City',
      'country' => 'Country',
      'streetnumber' => 'Nr',
    ),
    'modaltitle' => 'Add address',
    'title' => 'Enter the address',
  ),
);
