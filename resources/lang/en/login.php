<?php

return array (
  'register' => 
  array (
    'modaltitle' => 'Register to FileBridge',
    'title' => 'Please Sign Up It\'s free to try.',
    'label' => 
    array (
      'firstname' => 'Firstname',
      'lastname' => 'Lastname',
      'displayname' => 'Display name',
      'company' => 'Company',
      'vatnumber' => 'VAT number',
      'email' => 'Email',
      'password' => 'Password',
      'confirmpwd' => 'Confirm password',
      'Iagree' => 'I agree',
      'gtc1' => 'By chercking \'I Agree\', you will be able to',
      'gtc2' => 'and you then agree to the',
      'gtc3' => 'Terms and Conditions',
      'gtc4' => 'set out by this site, including our Cookie Use.',
    ),
    'button' => 
    array (
      'register' => 'Register',
    ),
    'link' => 
    array (
      'alreadymember' => 'I am already a member',
    ),
  ),
  'lostpwd' => 
  array (
    'modaltitle' => 'Lost your password? Request a new one.',
    'button' => 
    array (
      'request' => 'Request a new password',
      'login' => 'Return to login',
      'register' => 'Not Registered ?',
    ),
  ),
  'login' => 
  array (
    'modaltitle' => 'Log in to HookToTheCloud',
    'title' => 
    array (
      'email' => 'Please enter your username',
      'password' => 'Please enter your password',
    ),
    'placeholder' => 
    array (
      'email' => 'yourname@example.org',
      'password' => 'Password',
    ),
    'label' => 
    array (
      'rememberme' => 
      array (
        'help' => '(if this is a private computer)',
        'check' => 'Remember Me',
      ),
    ),
    'button' => 
    array (
      'login' => 'Log in',
      'forgot' => 'Forgot password?',
      'register' => 'Register',
    ),
    'register' => 
    array (
      'title' => 'Register now for FREE',
      'bullet1' => 'Stop reading, start testing',
      'readmore' => 'Read more',
      'bullet2' => 'Setup 1 bridge and try',
      'bullet3' => '5 minutes registration',
      'bullet4' => 'up to 10 file transfers',
      'bullet5' => '30 days trial',
    ),
  ),
);
