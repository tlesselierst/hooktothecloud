<?php

return array (
  'header' => 
  array (
    'company_name' => 'Entreprise',
    'email' => 'E-mail',
    'firstname' => 'Prénom',
    'lastname' => 'Nom',
    'name' => 'Nom d\'affichage',
    'action' => 'Action',
  ),
  'btn' => 
  array (
    'edit' => 'Editer',
    'addnewuser' => 'Nouveau compte',
  ),
  'add' => 
  array (
    'button' => 
    array (
      'register' => 'Ajouter',
      'create' => 'Créer',
    ),
    'label' => 
    array (
      'company' => 'Entreprise',
      'confirmpwd' => 'Confirmer mot de passe',
      'displayname' => 'Nom d\'affichage',
      'email' => 'E-mail',
      'firstname' => 'Prénom',
      'lastname' => 'Nom',
      'password' => 'Mot de passe',
      'vatnumber' => 'N° de TVA',
    ),
    'modaltitle' => 'Ajouter un nouvel utilisateur',
    'title' => 'Remplissez tous les champs. L\'utilisateur recevra un e-mail',
  ),
  'edit' => 
  array (
    'button' => 
    array (
      'save' => 'Sauvegarder',
    ),
    'modaltitle' => 'Editer l\'utilisateur',
    'title' => 'Changer les details de cet utilisateur',
    'leaveemptypassword' => 'Pour changer le mot de passe, veuillez remplir les champs. Si vous ne voulez pas changer le mot de passe, laissez les champs vides.',
    'changeEmail' => 'Changer l\'e-mail',
  ),
  'ecit' => 
  array (
    'changeEmail' => 'Changer l\'e-mail',
  ),
  'leaveemptypassword' => 'Pour changer le mot de passe, veuillez remplir les champs. Si vous ne voulez pas changer le mot de passe, laissez les champs vides.',
);
