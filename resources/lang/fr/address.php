<?php

return array (
  'add' => 
  array (
    'label' => 
    array (
      'street' => 'Rue',
      'zip' => 'CP',
      'city' => 'Ville',
      'country' => 'Pays',
      'streetnumber' => 'N°',
    ),
    'modaltitle' => 'Ajouter une addresse',
    'title' => 'Détaillez l\'adresse',
  ),
);
