<?php

return array (
  'header' => 
  array (
    'enterprise_name' => 'Enterprise',
    'vatnumber' => 'Numéro TVA',
    'cbo' => 'Numéro CBE',
    'invoice_address_id' => 'Adresse de facturation',
    'action' => 'Action',
  ),
  'btn' => 
  array (
    'addNewEnterprise' => 'Nouvelle enterprise',
    'edit' => 'Editer',
  ),
  'title' => 
  array (
    'addNewEnterprise' => 'Ajouter une nouvelle enteprise',
  ),
);
