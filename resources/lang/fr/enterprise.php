<?php

return array (
  'add' => 
  array (
    'button' => 
    array (
      'create' => 'Créer',
    ),
    'label' => 
    array (
      'cbo' => 'Numéro CBE',
      'enterprise_name' => 'Nom d\'entreprise',
      'vatnumber' => 'Numéro TVA',
    ),
    'modaltitle' => 'Ajouter une entreprise',
    'title' => 'Ajouter une enterprise a votre compte',
    'tooltip' => 
    array (
      'vatnumber' => 'Il faut checker TVA? pour ajouer le niuméro de TVA',
    ),
  ),
  'edit' => 
  array (
    'VATpresent' => 'TVA?',
  ),
);
