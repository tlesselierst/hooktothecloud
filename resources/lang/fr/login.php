<?php

return array (
  'register' => 
  array (
    'title' => 'Essayez-donc. L\'inscription ne coûte <b>rien.</b>',
    'modaltitle' => 'Activer un compte sur FileBridge',
    'label' => 
    array (
      'firstname' => 'Prénom',
      'lastname' => 'Nom de famille',
      'displayname' => 'Nom d\'utilisateur',
      'company' => 'Entreprise',
      'vatnumber' => 'Numéro de TVA',
      'email' => 'E-mail',
      'password' => 'Mot de passe',
      'confirmpwd' => 'Répétez le mot de passe',
      'Iagree' => 'Je suis d\'accord',
      'gtc1' => 'En cliquant \'Je suis d\'accord\' le button',
      'gtc2' => 'devient actif et vous vous déclarez d\'accord avec',
      'gtc3' => 'les conditions générales',
      'gtc4' => 'de ce site, ainsi que pour l\'emploi des cookie',
    ),
    'button' => 
    array (
      'register' => 'Souscrire',
    ),
    'link' => 
    array (
      'alreadymember' => 'Je suis déjà membre',
    ),
  ),
  'lostpwd' => 
  array (
    'modaltitle' => 'Oublié votre mot de passe? Demandez-en un nouveau ici',
    'button' => 
    array (
      'request' => 'Envoyez le nouveau mot de passe',
      'login' => 'S\'identifier',
      'register' => 'Pas encore de compte?',
    ),
  ),
  'login' => 
  array (
    'modaltitle' => 'Se logger sur HookToTheCloud',
    'title' => 
    array (
      'email' => 'Entrez ici votre email',
      'password' => 'Entrez ici votre mot de passe.',
    ),
    'placeholder' => 
    array (
      'email' => 'email@organisation.org',
      'password' => 'Mot de passe',
    ),
    'label' => 
    array (
      'rememberme' => 
      array (
        'help' => '(seulement s\'il s\'agit s\'un pc privé)',
        'check' => 'Se souvenir de moi',
      ),
    ),
    'button' => 
    array (
      'login' => 's\'identifier',
      'forgot' => 'Oublié le mot de passe',
      'register' => 'Enregistrer un compte',
    ),
    'register' => 
    array (
      'title' => 'Enregistrez-vous  GRATUITEMENT',
      'bullet1' => 'Assez lu, essayez-le maintenant',
      'readmore' => 'Lire plus',
      'bullet2' => 'Configurez 1 bridge',
      'bullet3' => 'Enregistrez-vous en moins de 5 min',
      'bullet4' => 'Transférez jusqu\'à 10 fichiers',
      'bullet5' => '30 jours d\'essai',
    ),
  ),
);
