<?php

return array (
  'slide1' => 
  array (
    'title' => 'HooktotheCloud',
    'oneliner' => 'Liez votre infrastructure au "cloud"',
    'btn' => 
    array (
      'signup' => 's\'Enregister',
      'login' => 's\'Identifier',
    ),
  ),
  'slide2' => 
  array (
    'title' => 'Démarrez avec HooktotheCloud',
    'oneliner' => 'et commencez l\'échange de documents immédiatement',
    'btn' => 
    array (
      'startconfig' => 'Définisez votre premier routage',
    ),
  ),
  'slide3' => 
  array (
    'title' => 'Support @ HooktotheCloud',
    'oneliner' => 'Et faites-vous aider par la commnauté',
    'btn' => 
    array (
      'action' => 'Accédez à notre organisation de support',
    ),
  ),
  'previous' => 'précédent',
  'next' => 'prochain',
);
