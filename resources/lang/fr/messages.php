<?php

return array (
  'logged_out' => 'Merci. Vous n\'êtes plus authentifié.',
  'error_registered' => 'L\'enregistrement n\'a pas réussi. Veuillez vérifier le formulaire d\'insccription.',
  'logged_in' => 'Vous êtes identifié maintenant. Bienvenue!',
  'registered' => 'Votre enregisterement a réussi. Vous pouvez maintenant vous identifier',
  'notloggedin' => 'La page que vous voulez acceder est sécurisé. Veuillez-vous identifier.',
  'noaccess' => 'Vous n\'avez pas le droit d\'accéder à la page.',
  'error_creating_user' => 'L\'utilisateur n\'a pas pu être cré. Veuillez vérifier le formulaire.',
  'user_created' => 'L\'utilisateur a été créé. Il recevra un e-mail d\'invitation',
  'error_updating_user' => 'Les informations de l\'utilisateur n\'on pas pu être changés',
  'user_updated' => 'Les détails de l\'utilisateur ont été mis à jour.',
  'profileupdate_done' => 'Vous  avez mis à jour votre profile',
  'profileupdate_failed' => 'Votre profile n\'a pas pu être mis à jour.',
  'profileupdate_failed_wrong_profile' => 'Il y a eu une erreur de profile. La mise a jour à été annulée.',
  'enterprise_created' => 'L\'entreprise a été créée.',
  'error_creating_enterprise' => 'L\'entreprise n\'a pa pu être créée . Veuillez regarder le formulaire',
);
