<?php

return array (
  'header' => 
  array (
    'company_name' => 'Onderneming',
    'email' => 'E-mail',
    'firstname' => 'Voornaam',
    'lastname' => 'Achternaam',
    'name' => 'Weergavenaam',
    'action' => 'Actie',
  ),
  'btn' => 
  array (
    'edit' => 'Bewerken',
    'addnewuser' => 'Nieuwe gebruiker',
  ),
  'add' => 
  array (
    'modaltitle' => 'Een nieuwe gebruiker toevoegen',
    'title' => 'Vul alle velden in, de gebruiker ontvangt dan een email',
    'label' => 
    array (
      'firstname' => 'Voornaam',
      'lastname' => 'Achternaam',
      'displayname' => 'Weergavenaam',
      'company' => 'Onderneming',
      'vatnumber' => 'BTW nummer',
      'email' => 'E-mail',
      'password' => 'Wachtwoord',
      'confirmpwd' => 'Wachtwoord bevestigen',
    ),
    'button' => 
    array (
      'register' => 'Toevoegen',
      'create' => 'Aanmaken',
    ),
  ),
  'edit' => 
  array (
    'modaltitle' => 'Gebruikerdetails aanpassen',
    'title' => 'Gebruik dit formulier om de gebruikerdetails aan te passen',
    'button' => 
    array (
      'save' => 'Bewaren',
    ),
    'changeEmail' => 'E-mail veranderen',
    'leaveemptypassword' => 'Als je he wachtwoord wil aanpassen, vul dan de onderstaande velden in. Wil je het wachtwoord niet veranderen, geef dan niets in.',
  ),
  'ecit' => 
  array (
    'changeEmail' => 'E-mail veranderen',
  ),
  'leaveemptypassword' => 'Als je he wachtwoord wil aanpassen, vul dan de onderstaande velden in. Wil je het wachtwoord niet veranderen, geef dan niets in.',
);
