<?php

return array (
  'add' => 
  array (
    'modaltitle' => 'Onderneming toevoegen',
    'title' => 'Een onderneming toevoegen aan uw account',
    'label' => 
    array (
      'enterprise_name' => 'Naam onderneming',
      'vatnumber' => 'BTW nummer',
      'cbo' => 'KBO nummer',
    ),
    'button' => 
    array (
      'create' => 'Aanmaken',
    ),
    'tooltip' => 
    array (
      'vatnumber' => 'Gelieve BTW? aan te vinken om BTW nummer te kunnen toevoegen.',
    ),
  ),
  'edit' => 
  array (
    'VATpresent' => 'BTW?',
  ),
);
