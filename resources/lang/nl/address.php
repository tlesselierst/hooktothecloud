<?php

return array (
  'add' => 
  array (
    'label' => 
    array (
      'street' => 'Straat',
      'zip' => 'Postcode',
      'city' => 'Plaats',
      'country' => 'Land',
      'streetnumber' => 'N°',
    ),
    'modaltitle' => 'Voeg een adres toe',
    'title' => 'Adresdetails',
  ),
);
