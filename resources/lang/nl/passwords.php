<?php

return array (
  'password' => 'Wachtwoord moet minimaal zes tekens lang zijn en de wachtwoorden moeten overeenkomen.',
  'reset' => 'Het wachtwoord van uw account is gewijzigd.',
  'sent' => 'We hebben een e-mail verstuurd met instructies om een nieuw wachtwoord in te stellen.',
  'token' => 'Dit wachtwoord reset token is niet geldig.',
  'user' => 'Geen gebruiker bekend met dat e-mailadres.',
);
