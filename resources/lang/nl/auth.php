<?php

return array (
  'failed' => 'Deze combinatie van e-mailadres en wachtwoord is niet geldig.',
  'throttle' => 'Teveel mislukte login pogingen. Probeer het over :seconds seconden nogmaals.',
);
