<?php

return array (
  'header' => 
  array (
    'enterprise_name' => 'Onderneming',
    'vatnumber' => 'BTW nummer',
    'cbo' => 'KBO nummer',
    'invoice_address_id' => 'Facturatieadres',
    'action' => 'Acties',
  ),
  'btn' => 
  array (
    'addNewEnterprise' => 'Nieuwe onderneming',
    'edit' => 'Bewerken',
  ),
  'title' => 
  array (
    'addNewEnterprise' => 'Nieuwe onderneming toevoegen',
  ),
);
