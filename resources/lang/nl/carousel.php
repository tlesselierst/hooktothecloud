<?php

return array (
  'next' => 'volgende',
  'previous' => 'vorige',
  'slide1' => 
  array (
    'btn' => 
    array (
      'login' => 'Aanloggen',
      'signup' => 'Registeren',
    ),
    'oneliner' => 'Maak de brug tussen het huidige infrastructuur en de "Cloud"',
    'title' => 'HooktotheCloud',
  ),
  'slide2' => 
  array (
    'btn' => 
    array (
      'startconfig' => 'Maak je eerste route aan',
    ),
    'oneliner' => 'en vervoer onmiddellijk documenten',
    'title' => 'Kickstart nu met HooktotheCloud',
  ),
  'slide3' => 
  array (
    'btn' => 
    array (
      'action' => 'Toegang tot onze supportorganisatie',
    ),
    'oneliner' => 'en laat je onmiddellijk verder helpen door onze community',
    'title' => 'Support @ HooktotheCloud',
  ),
);
