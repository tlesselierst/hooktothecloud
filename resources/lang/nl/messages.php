<?php

return array (
  'enterprise_created' => 'De onderneming werd toegevoegd',
  'error_creating_enterprise' => 'De onderneming kon niet worden aangemaakt. Kijk naar het formulier om de reden te vinden',
  'logged_out' => 'Dank u wel. U bent niet meer aangelogd.',
  'error_registered' => 'De registratie is niet gelukt. Kijk de informatie na op het formulier.',
  'logged_in' => 'Welkom, U bent nu ingelopgd.',
  'registered' => 'Uw registratie is gelukt. U kan nu inloggen',
  'notloggedin' => 'De pagina is beveiligd. Gelieve eerst aan te loggen',
  'noaccess' => 'U heeft geen toegang tot deze pagina',
  'error_creating_user' => 'De gebruiker kon niet worden aangemaakt. Gelieve het formulier na te kijken.',
  'user_created' => 'De gebruiker werd aangemaakt. Hij zal een email ontvangen.',
  'error_updating_user' => 'De gebruiker kon niet worden aangepast.',
  'user_updated' => 'De gebruiker werd aangepast',
  'profileupdate_done' => 'Uw profiel werd aangepast.',
  'profileupdate_failed' => 'Uw profiel kon niet worden aangepast.',
  'profileupdate_failed_wrong_profile' => 'Uw profiel kwam niet overeen met de informatie die we hadden. De update is niet uitgevoerd.',
);
