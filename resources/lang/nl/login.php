<?php

return array (
  'register' => 
  array (
    'button' => 
    array (
      'register' => 'Registreren',
    ),
    'label' => 
    array (
      'company' => 'Onderneming',
      'confirmpwd' => 'Herhaal wachtwoord',
      'displayname' => 'Gebruikersnaam',
      'email' => 'E-mail',
      'firstname' => 'Voornaam',
      'gtc1' => 'Door op \'ik ga akkoord\' te klikken , wordt de',
      'gtc2' => 'knop actief en verklaart u zich akkoord met de',
      'gtc3' => 'algemene voorwaarden',
      'gtc4' => 'van deze applicatie, alsook het gebruik van cookies',
      'Iagree' => 'Ik ga akkoord',
      'lastname' => 'Familienaam',
      'password' => 'Wachtwoord',
      'vatnumber' => 'BTW nummer',
    ),
    'link' => 
    array (
      'alreadymember' => 'Ik heb reeks een account',
    ),
    'modaltitle' => 'Een account activeren op FileBridge',
    'title' => 'Het kost je helemaal niets om je in te schrijven.',
  ),
  'lostpwd' => 
  array (
    'button' => 
    array (
      'login' => 'Terug naar inlogscherm',
      'register' => 'Nog geen account?',
      'request' => 'Stuur het nieuw wachtwoord op',
    ),
    'modaltitle' => 'Wachtwoord kwijt? Vraag hier gewoon een nieuwe',
  ),
  'login' => 
  array (
    'button' => 
    array (
      'forgot' => 'Wachtwoord kwijt?',
      'login' => 'Inloggen',
      'register' => 'Account registreren',
    ),
    'label' => 
    array (
      'rememberme' => 
      array (
        'help' => '( als dit een persoonlijke pc is)',
        'check' => 'Onthou mij',
      ),
    ),
    'modaltitle' => 'Login op HookToTheCloud',
    'placeholder' => 
    array (
      'email' => 'email@organisatie.org',
      'password' => 'Wachtwoord',
    ),
    'register' => 
    array (
      'bullet1' => 'Genoeg gelezen, probeer het zelf',
      'readmore' => 'Lees meer',
      'title' => 'Registreer nu  GRATIS',
      'bullet2' => 'Maak 1 bridge',
      'bullet3' => 'Registreer in 5 mins',
      'bullet4' => 'Verwerk tot 10 bestanden',
      'bullet5' => '30 dagen test',
    ),
    'title' => 
    array (
      'email' => 'Vul hier je email in',
      'password' => 'Vul hier je wachtwoord in',
    ),
  ),
);
