<!--Carousel Wrapper-->
    <div id="video-carousel-example2" class="carousel slide carousel-fade" data-ride="carousel">
        <!--Indicators-->
        <ol class="carousel-indicators">
            <li data-target="#video-carousel-example2" data-slide-to="0"></li>
            <li data-target="#video-carousel-example2" data-slide-to="1"></li>
            <li data-target="#video-carousel-example2" data-slide-to="2"></li>
        </ol>
        <!--/.Indicators-->

        <!--Slides-->
        <div class="carousel-inner" role="listbox">
            <!--First slide-->
            <div class="carousel-item active">
                <!--Mask color-->
                <div class="view hm-black-light">

                    <!--Video source-->
                    <video class="video-full" autoplay loop>
                        <source src="{{ asset('vid/Tropical.mp4') }}" type="video/mp4" />
                    </video>
                    <div class="full-bg-img"></div>
                </div>

                <!--Caption-->
                <div class="carousel-caption">
                    <div class="flex-center animated fadeIn">
                        <ul>
                            <li>
                                <h1 class="h1-responsive wow fadeInDown" data-wow-delay="0.2s">@lang('carousel.slide1.title')</h1></li>
                            <li>
                                <p>@lang('carousel.slide1.oneliner')</p>
                            </li>
                            <li>
                                <a href="#register-overlay" data-toggle="modal" data-target="#register-overlay" class="btn btn-primary btn-lg" rel="nofollow">@lang('carousel.slide1.btn.signup')</a>
                                <a href="#login-overlay" data-toggle="modal" data-target="#login-overlay"  class="btn btn-default btn-lg" rel="nofollow">@lang('carousel.slide1.btn.login')</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--Caption-->
            </div>
            <!--/First slide-->

            <!--Second slide-->
            <div class="carousel-item">
                <!--Mask color-->
                <div class="view hm-black-light">
                    <!--Video source-->
                    <video class="video-full" autoplay loop>
                        <source src="{{ asset('vid/Sail-Away.mp4') }}" type="video/mp4" />
                    </video>
                    <div class="mask"></div>
                </div>

                <!--Caption-->
                <div class="carousel-caption">
                    <div class="flex-center animated fadeInDown">
                        <ul>
                            <li>
                                <h1 class="h1-responsive">@lang('carousel.slide2.title')</h1>
                            </li>
                            <li>
                                <p>@lang('carousel.slide2.oneliner')</p>
                            </li>
                            <li>
                                <a target="_blank" href="#" class="btn btn-primary btn-lg" rel="nofollow">@lang('carousel.slide2.btn.startconfig')</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/.Caption-->
            </div>
            <!--/Second slide-->

            <!--Third slide-->
            <div class="carousel-item">
                <!--Mask color-->
                <div class="view hm-black-strong">
                    <!--Video source-->
                    <video class="video-full" autoplay loop>
                        <source src="{{ asset('vid/Agua-natural.mp4') }}"  type="video/mp4" />
                    </video>
                    <div class="mask"></div>
                </div>

                <!--Caption-->
                <div class="carousel-caption">
                    <div class="flex-center animated fadeInDown">
                        <ul>
                            <li>
                                <h1 class="h1-responsive">@lang('carousel.slide3.title')</h1></li>
                            <li>
                                <p>@lang('carousel.slide3.oneliner')</p>
                            </li>
                            <li>
                                <a target="_blank" href="#" class="btn btn-default btn-lg" rel="nofollow">@lang('carousel.slide3.btn.action')</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--/.Caption-->
            </div>
            <!--/Third slide-->
        </div>
        <!--/.Slides-->

        <!--Controls-->
        <a class="carousel-control-prev" href="#video-carousel-example2" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">@lang('carousel.previous')</span>
        </a>
        <a class="carousel-control-next" href="#video-carousel-example2" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">@lang('carousel.next')</span>
        </a>
        <!--/.Controls-->
    </div>
    <!--/.Carousel Wrapper-->