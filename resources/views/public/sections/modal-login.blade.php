<!--Modal: Login Form-->
<div class="modal fade" id="login-overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header  mdb-color darken-4 white-text">
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="title"><i class="fa fa-user"></i>@lang('login.login.modaltitle')</h4>
            </div>

            
            <form id="loginForm" method="POST" action="{{ LaravelLocalization::getLocalizedURL(null,'/login') }}" novalidate="novalidate" >
            	
            	{!! csrf_field() !!}
            	<!--Body-->
            	<div class="modal-body container-fluid"> 

            		<div class="row">
                 
	            		<!-- left column -->
	                 	<div class="col-md-6">  

	                 		<!-- login/email -->
			            	<div class="md-form form-sm">
			                    <i class="fa fa-envelope prefix"></i>
			                    <input type="email"
			                           id="email"
			                           name="email"
			                           class="form-control validate"
			                           placeholder="@lang('login.login.title.email')"
			                           required=""
			                           autofocus=""
			                           tabindex="1">
			                </div>

			                <!-- password -->
			                <div class="md-form form-sm">
			                    <i class="fa fa-lock prefix"></i>
			                    <input type="password"
                                       class="form-control"
                                       id="password"
                                       name="password"
                                       class="form-control validate"
			                           placeholder="@lang('login.login.placeholder.password')"
                                       required=""
                                       tabindex="2">
			                </div>

			                <!-- remember me  -->
			                <div>
                                <p class="help-block">@lang('login.login.label.rememberme.help')</p>
                                <fieldset class="form-group">
		                            <input type="checkbox"
                                           name="remember"
                                           tabindex="3"
                                           id="remember">
		                            <label for="remember">@lang('login.login.label.rememberme.check')</label>
		                        </fieldset>
                            </div>

			            </div>

			            <!-- right column -->
			            <div class="col-xmd-6">
	                      <p class="lead">@lang('login.login.register.title')</p>
	                      <ul class="list-unstyled" style="line-height: 2">
	                          <li><span class="fa fa-check text-success"></span>@lang('login.login.register.bullet1')</li>
	                          <li><span class="fa fa-check text-success"></span>@lang('login.login.register.bullet2')</li>
	                          <li><span class="fa fa-check text-success"></span>@lang('login.login.register.bullet3') </li>
	                          <li><span class="fa fa-check text-success"></span>@lang('login.login.register.bullet4')</li>
	                          <li><span class="fa fa-check text-success"></span>@lang('login.login.register.bullet5')</li>
	                          <li><a href="/read-more/"><u>@lang('login.login.register.readmore')</u></a></li>
	                      </ul>
						</div>
			        </div>
            	</div>

	            <!--Footer-->
	            <div class="modal-footer">
	    			<button type="submit" class="btn btn-primary waves-effect" tabindex="4">@lang('login.login.button.login')<i class="fa fa-sign-in ml-1"></i> </button>
	    			 <button type="button" class="btn btn-default waves-effect"" id="forgot-trigger" tabindex="5">@lang('login.login.button.forgot')</button>
					<button type="button" class="btn btn-secondary waves-effect ml-auto" tabindex="5" id="join-trigger">@lang('login.login.button.register')</button>
				</div>

	            </div>
        	</form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Login Form-->