<!--Modal: Lost password Form-->
<div class="modal fade" id="password-overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header  mdb-color darken-4 white-text">
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="title"><i class="fa fa-lock"></i>@lang('login.lostpwd.modaltitle')</h4>
            </div>

            
            <form accept-charset="UTF-8" role="form" id="login-form" method="post">
            	
            	{!! csrf_field() !!}
            	<!--Body-->
            	<div class="modal-body container-fluid"> 
            		<div class="row">
                 
	            		<!-- left column -->
	                 	<div class="col-md-12">  

			                <!-- password -->
			                <div class="md-form form-sm">
			                    <i class="fa fa-envelope prefix"></i>
			                    <input class="form-control"
					                 placeholder="@lang('login.register.label.email')"
					                 name="email"
					                 type="email"
					                 required=""
					                 autofocus=""
					                 tabindex="1">
			                </div>
			            </div><!-- end left column -->
			        </div><!-- end row -->
            	</div>

	            <!--Footer-->
	            <div class="modal-footer">
	    			<button type="submit" class="btn btn-primary waves-effect" tabindex="4"> @lang('login.lostpwd.button.request')<i class="fa fa-sign-in ml-1"></i> </button>
					<button type="button" class="btn btn-secondary waves-effect ml-auto" tabindex="5" data-dismiss="modal" id="back-login"> @lang('login.lostpwd.button.login')</button>
				</div>

	            </div>
        	</form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Login Form-->