<!--Navbar-->
    <nav class="navbar navbar-toggleable-md navbar-dark fixed-top scrolling-navbar">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
             <a class="navbar-brand" href="/">@lang('menu.application_name')</a>
            <div class="collapse navbar-collapse" id="navbarNav1">
                <ul class="navbar-nav mr-auto smooth-scroll"" >
                    <li class="nav-item active"><a class="nav-link">Home <span class="sr-only">(current)</span></a> </li>
                    <li class="nav-item"><a href="#best-features"" class="nav-link">@lang('menu.features')</a></li>
                    <li class="nav-item"><a href="#pricing" class="nav-link">@lang('menu.pricing')</a></li>
                    <li class="nav-item"><a href="{{ url('about') }}" class="nav-link">@lang('menu.about')</a></li>
                    <li class="nav-item"><a href="#contact" class="nav-link">@lang('menu.contactus')</a></li>
                </ul>

                <ul class="navbar-nav mr-auto navbar-right">
                    <li class="nav-item dropdown btn-group">
                        <a href="#" class="nav-link dropdown-toggle" id="dropdownLanguage" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{   LaravelLocalization::getCurrentLocaleNative() }}</a>
                        <div class="dropdown-menu dropdown" aria-labelledby="dropdownLanguage">
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}" class="dropdown-item">{{ $properties['native'] }}</a>
                            @endforeach
                        </div>
                    </li>

                    @if(!Auth::check())
                        <li class="nav-item">
                            <a class="nav-link"  href="#login-overlay" data-toggle="modal" data-target="#login-overlay" ><i class="fa fa-sign-in"></i> @lang('menu.login')</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#register-overlay" data-toggle="modal" data-target="#register-overlay" ><i class="fa fa-registered"></i>  @lang('menu.register')</a>
                        </li>
                    @else
                        <li class="nav-item dropdown btn-group">
                            <a href="#" class="nav-link dropdown-toggle" id="dropdownUser" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <i class="fa fa-user"></i> <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <a href="{{ route('profile') }}" class="dropdown-item">@lang('menu.profile')</a></li>
                                <a href="{{ url('/users/filein') }}" class="dropdown-item">@lang('menu.receiving')</a></li>
                                <a href="{{ url('/users/fileout') }}" class="dropdown-item">@lang('menu.sending')</a></li>
                                <a href="{{ url('/logout') }}"   class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> @lang('menu.logout')</a>
                                    <form id="logout-form" 
                                    action="{{ LaravelLocalization::getLocalizedURL(null,'/logout') }}" 
                                    method="POST" 
                                    style="display: none;">
                                    {{ csrf_field() }}
                                    </form>
                            </ul>
                        </li>

                        <li class="nav-item">
                            
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <!--/.Navbar-->
