<!--Modal: Register Form-->
<div class="modal fade" id="register-overlay" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header  mdb-color darken-4 white-text">
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="title"><i class="fa fa-user-plus"></i> @lang('login.register.modaltitle')</h4>
            </div>

            <!--Body-->
            <form class="form-horizontal" role="form" id="formRegister" method="POST" action="{{ LaravelLocalization::getLocalizedURL(null,'/register') }}">
              <div class="modal-body container-fluid">   
                  {!! csrf_field() !!}
                  <p class="lead">@lang('login.register.title')</p>
                  
                   <!-- First and last name -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                          <input type="text"
                               name="first_name"
                               id="first_name"
                               class="form-control validate required"
                               tabindex="1"
                               autofocus="">
                          <label for="first_name">@lang('login.register.label.firstname')</label>
                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                        <input type="text"
                               name="last_name"
                               id="last_name"
                               class="form-control validate required"
                               tabindex="2">
                        <label for="last_name">@lang('login.register.label.lastname')</label>
                      </div>
                    </div>
                  </div>

                  <!-- Display name -->
                  <div class="row">
                    <div class="col-md-12">
                       <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                        <input type="text"
                               name="name"
                               id="name"
                               class="form-control"
                               tabindex="3">
                        <label for="name">@lang('login.register.label.displayname')</label>
                      </div>
                    </div>
                  </div>
              
                   <!-- Company and VAT number -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="company_name"
                               id="company_name"
                               class="form-control validate"
                               tabindex="4">
                        <label for="company_name">@lang('login.register.label.company')</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                        <i class="fa fa-address-card-o darken-4 prefix"></i>
                        <input type="text"
                               name="vatnumber"
                               id="vatnumber"
                               class="form-control validate"
                               tabindex="5">
                        <label for="vatnumber">@lang('login.register.label.vatnumber')</label>
                      </div>
                    </div>
                  </div>

                   <!-- email  -->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="md-form form-sm">
                        <i class="fa fa-envelope  darken-4 prefix"></i>
                        <input type="email"
                               name="email"
                               id="email"
                               class="form-control"
                               tabindex="6">
                        <label for="email">@lang('login.register.label.email')</label>
                      </div>
                    </div>
                  </div>

                   <!-- password -->
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-lock  darken-4 prefix"></i>
                          <input type="password"
                                 name="password"
                                id="password"
                                class="form-control"
                                tabindex="7">
                          <label for="password">@lang('login.register.label.password')</label>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                       <div class="md-form form-sm">
                          <i class="fa fa-lock  darken-4 prefix"></i>
                          <input type="password"
                                name="password_confirmation"
                                id="password_confirmation"
                                 class="form-control"
                                tabindex="8">
                          <label for="password_confirmation">@lang('login.register.label.confirmpwd')</label>
                      </div>
                    </div>
                  </div>

                  <!-- agree GTC -->
                  <div class="row">
                    <div class="col-xs-4 col-sm-3 col-md-3">
                       <span class="button-checkbox">
                        <fieldset class="form-group">
                            <input type="checkbox"
                               name="t_and_c"
                               id="t_and_c"
                               value="1"
                               tabindex="9">
                            <label for="t_and_c">@lang('login.register.label.Iagree')</label>
                        </fieldset>
                      </span>
                    </div>
                
                    <div class="col-xs-8 col-sm-9 col-md-9">
                      @lang('login.register.label.gtc1') <strong class="badge badge-primary">@lang('login.register.button.register')</strong> @lang('login.register.label.gtc2') <a href="t_and_c" data-toggle="modal" data-target="#t_and_c_m">@lang('login.register.label.gtc3')</a> @lang('login.register.label.gtc4')
                    </div>
                  </div>

              </div>
            
              <!--Footer-->
              <div class="modal-footer">
                <button type="submit"
                        class="btn btn-outline-primary waves-effect"
                        id="registering-button"
                        tabindex="10"
                        disabled="true">
                  @lang('login.register.button.register')
                </button>
                <button type="button"
                        class="btn btn-secondary waves-effect ml-auto"
                        id="login-trigger"> 
                  @lang('login.register.link.alreadymember')
                </button>
              </div>
          </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Register Form-->