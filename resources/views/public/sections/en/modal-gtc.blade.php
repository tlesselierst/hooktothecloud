<!-- Modal -->
<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
			</div>
			<div class="modal-body">
				<h4>Terms and Conditions</h4>
				<h5>Terms and Conditions ("Terms")</h5>
					<p>Last updated: 20-feb-2017</p>
					<p>Please read these Terms and Conditions ("Terms", "Terms and Conditions") carefully before using the website and the mobile application (the "Service") operated by us.</p>
					<p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>
					<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
				<h5>Purchases</h5>
					<p>If you wish to purchase any product or service made available through the Service ("Purchase"), you may be asked to supply certain information relevant to your Purchase including, without limitation, your details.</p>
				<h5>Subscriptions</h5>
					<p>Some parts of the Service are billed on a subscription basis ("Subscription(s)"). You will be billed in advance on a recurring </p>
				<h5>Changes</h5>
					<p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 (change this) days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion.</p>
				<h5>Contact Us</h5>
					<p>If you have any questions about these Terms, please contact us.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->