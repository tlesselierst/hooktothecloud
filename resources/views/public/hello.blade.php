@extends('public.mdb_template')

@section('header')
 @include('public.sections.caroussel')
@endsection

@section('main')
<!--Main layout-->

<div class="container">

        <div class="divider-new">
        <h2 class="h2-responsive">Hook to the Cloud</h2>
    </div> 

    <section id="hooktothecloud-what">
        <!--First row-->
        <div class="row">
            <div class="col-md-7">
            <!--Featured image -->
                <div class="view overlay hm-white-light z-depth-1-half">
                    <img src="https://mdbootstrap.com/images/proffesions/slides/socialmedia/img%20(2).jpg" class="img-fluid " alt="">
                    <div class="mask">
                    </div>
                </div>
                <br>
            </div>

            <!--Main information-->
            <div class="col-md-5">
                <h2 class="h2-responsive">We are professionals</h2>
                <hr>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis pariatur quod ipsum atque quam dolorem voluptate officia sunt placeat consectetur alias fugit cum praesentium ratione sint mollitia, perferendis natus quaerat!</p>
                <a href="" class="btn btn-primary">Get it now!</a>
            </div>
        </div>
        <!--/.First row-->

        <hr class="extra-margins">

        <div class="divider-new">
            <h2 class="h2-responsive wow fadeInDown">Best Features</h2>
        </div> 
    </section>

   <!--Section: Best features-->
<section id="best-features">
    <div class="row">
        <!--First columnn-->
        <div class="col-md-4">
            <!--Card-->
            <div class="card wow fadeInUp">

                <!--Card image-->
                <div class="view overlay hm-white-slight">
                    <img src="https://mdbootstrap.com/images/regular/city/img%20(2).jpg" class="img-fluid" alt="">
                    <a href="#">
                        <div class="mask waves-light"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-center">
                    <!--Title-->
                    <h4 class="card-title">Card title</h4>
                    <hr>
                    <!--Text-->
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->
        </div>
        <!--First columnn-->

        <!--Second columnn-->
        <div class="col-md-4">
            <!--Card-->
            <div class="card wow fadeInUp" data-wow-delay="0.2s">

                <!--Card image-->
                <div class="view overlay hm-white-slight">
                    <img src="https://mdbootstrap.com/images/regular/city/img%20(4).jpg" class="img-fluid" alt="">
                    <a href="#">
                        <div class="mask waves-light"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-center">
                    <!--Title-->
                    <h4 class="card-title">Card title</h4>
                    <hr>
                    <!--Text-->
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->
        </div>
        <!--Second columnn-->

        <!--Third columnn-->
        <div class="col-md-4">
            <!--Card-->
            <div class="card wow fadeInUp" data-wow-delay="0.4s">

                <!--Card image-->
                <div class="view overlay hm-white-slight">
                    <img src="https://mdbootstrap.com/images/regular/city/img%20(8).jpg" class="img-fluid" alt="">
                    <a href="#">
                        <div class="mask waves-light"></div>
                    </a>
                </div>
                <!--/.Card image-->

                <!--Card content-->
                <div class="card-block text-center">
                    <!--Title-->
                    <h4 class="card-title">Card title</h4>
                    <hr>
                    <!--Text-->
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
                <!--/.Card content-->

            </div>
            <!--/.Card-->
        </div>
        <!--Third columnn-->
    </div>
</section>
<!--/Section: Best features-->

<div class="divider-new">
        <h2 class="h2-responsive wow fadeInDown">Pricing</h2>
    </div>

<!--Section: Pricing v.1-->
<section id="pricing">

    <!--Section heading-->
    <h1 class="section-heading">Our pricing plans v.1</h1>
    <!--Section description-->
    <p class="section-description">Get immediate access to the cloud apps and make the link to your SFTP</p>

    <!--First row-->
    <div class="row">

        <!--First column-->
        <div class="col-lg-4 col-md-6 m-b-r">

            <!--Pricing card-->
            <div class="card pricing-card">
                <!--Price-->
                <div class="price header blue">
                    <h1>10</h1>
                    <div class="version">
                        <h5>Basic</h5>
                    </div>
                </div>
                <!--/.Price-->

                <!--Features-->
                <div class="card-block striped">
                    <ul>
                        <li>
                            <p><i class="fa fa-check"></i> 10 MB Of Storage</p>
                        </li>
                        <li>
                            <p><i class="fa fa-check"></i> 1 Route</p>
                        </li>
                        <li>
                            <p><i class="fa fa-times"></i> 24h Tech Support</p>
                        </li>
                        <li>
                            <p><i class="fa fa-times"></i> User Management </p>
                        </li>
                    </ul>

                    <button class="btn btn-primary">Buy now</button>
                </div>
                <!--/.Features-->

            </div>
            <!--/.Pricing card-->

        </div>
        <!--/First column-->

        <!--Second column-->
        <div class="col-lg-4 col-md-6 m-b-r">

            <!--Pricing card-->
            <div class="card pricing-card">
                <!--Price-->
                <div class="price header indigo">
                    <h1>49</h1>
                    <div class="version">
                        <h5>Pro</h5>
                    </div>
                </div>
                <!--/.Price-->

                <!--Features-->
                <div class="card-block striped">
                    <ul>
                        <li>
                            <p><i class="fa fa-check"></i> 1 GB Of Storage</p>
                        </li>
                        <li>
                            <p><i class="fa fa-check"></i> 4 Routes</p>
                        </li>
                        <li>
                            <p><i class="fa fa-check"></i> 24h Tech Support</p>
                        </li>
                        <li>
                            <p><i class="fa fa-times"></i> User Management </p>
                        </li>
                    </ul>

                    <button class="btn btn-primary">Buy now</button>
                </div>
                <!--/.Features-->

            </div>
            <!--/.Pricing card-->

        </div>
        <!--/Second column-->

        <!--Third column-->
        <div class="col-lg-4 col-md-6 m-b-r">
            <!--Pricing card-->
            <div class="card pricing-card">
                <!--Price-->
                <div class="price header deep-purple">
                    <h1>99</h1>
                    <div class="version">
                        <h5>Enterprise</h5>
                    </div>
                </div>
                <!--/.Price-->

                <!--Features-->
                <div class="card-block striped">
                    <ul>
                        <li>
                            <p><i class="fa fa-check"></i> 10 GB Of Storage</p>
                        </li>
                        <li>
                            <p><i class="fa fa-check"></i> 10 Routes</p>
                        </li>
                        <li>
                            <p><i class="fa fa-check"></i> 24h Tech Support</p>
                        </li>
                        <li>
                            <p><i class="fa fa-check"></i> User Management </p>
                        </li>
                    </ul>

                    <button class="btn btn-primary">Buy now</button>
                </div>
                <!--/.Features-->

            </div>
            <!--/.Pricing card-->
        </div>
        <!--/Third column-->

    </div>
    <!--/First row-->

</section>
<!--/Section: Pricing v.1-->


      <div class="divider-new">
        <h2 class="h2-responsive">Contact us</h2>
    </div>

    <!--Section: Contact-->
    <section id="contact">
        <div class="row">
            <!--First column-->
            <div class="col-md-8">

            </div>
            <!--/First column-->

            <!--Second column-->
            <div class="col-md-4">
                <ul class="text-center">
                    <li class="wow fadeInUp" data-wow-delay="0.2s"><i class="fa fa-map-marker"></i>
                        <p>New York, NY 10012, USA</p>
                    </li>

                    <li class="wow fadeInUp" data-wow-delay="0.3s"><i class="fa fa-phone"></i>
                        <p>+ 01 234 567 89</p>
                    </li>

                    <li class="wow fadeInUp" data-wow-delay="0.4s"><i class="fa fa-envelope"></i>
                        <p>contact@mdbootstrap.com</p>
                    </li>
                </ul>
            </div>
            <!--/Second column-->
        </div>
    </section>
    <!--Section: Contact-->


</div>
<!--/.Main layout-->

@endsection
