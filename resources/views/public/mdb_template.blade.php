<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Application to make the link between classic sftp protocol and the recent REST ">
    <meta name="author" content="Tanguy Lesseliers">


    <title>Hook to the Cloud | @yield('pagetitle')</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Material Design Bootstrap -->
    <link rel="stylesheet"  type="text/css" href="{{ asset('css/mdb.min.css') }}">

    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/httc.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    

    <!-- Start your project here-->
    <header>
        <!-- Topmenu -->
     
        @include('public.sections.navbar')
    </header>
    @yield('header')

    <main>
        @include('public.sections.flash-message')
        <!-- page content -->
        @yield('main')
        <!-- end content -->
    </main>
    @include('public.sections.modal-login')
    @include('public.sections.modal-register')
    @include('public.sections.modal-password')
    @include('public.sections.' .LaravelLocalization::getCurrentLocale() . '.modal-gtc')

    <footer>
        @yield('footer')
        <!--Copyright-->
        @include('public.sections.footer-bottom')
    </footer>
    <!-- /Start your project here-->
    

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/tether.min.js')}}"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js')}}"></script>

    <!--Animations initialization-->
    <script>
        new WOW().init();
    </script>

    <script type="text/javascript">
    

        //switch between login and register
        $("#join-trigger").click(function(){
          $('#login-overlay').modal('hide');
          $('#register-overlay').modal('show');
        });

        //switch between register and login
        $("#login-trigger").click(function(){
          $('#register-overlay').modal('hide');
          $('#login-overlay').modal('show');
        });

        //switch between login and forgot
        $("#forgot-trigger").click(function(){
          $('#login-overlay').modal('hide');
          $('#password-overlay').modal('show');
        });

        //from lost password to login
        $("#back-login").click(function(){
          $('#password-overlay').modal('hide');
          $('#login-overlay').modal('show');
        });

        //script to  manage the I Agree button
        $(function () {
            $('.button-checkbox').each(function () {

            // Settings
            var $widget = $(this),
            $checkbox = $widget.find('input:checkbox');

            // Event Handlers
            $checkbox.on('change', function () {
                updateDisplay();
            });

            // Actions
            function updateDisplay() {
                var isChecked = $checkbox.is(':checked');

                // Update the button's color
                if (isChecked) {
                    $('#registering-button').removeClass('btn-outline-primary');
                    $('#registering-button').addClass('btn-primary');
                    $('#registering-button').removeAttr('disabled');

                }
                else {
                    $('#registering-button').removeClass('btn-primary');
                    $('#registering-button').addClass('btn-outline-primary');
                    $('#registering-button').attr('disabled', 'disabled');
                }
            }

            // Initialization
            function init() {

                updateDisplay();

            }
            init();
            });

            $(document).on('submit', '#formRegister', function(e) {  
            e.preventDefault();
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                $('.alert-success').removeClass('hidden');
                $('#register-overlay').modal('hide');
                 toastr["info"]('{{ trans('messages.registered')}}');
                
            })
            .fail(function(data) {
                $.each(data.responseJSON, function (key, value) {
                    var input = '#formRegister input[name=' + key + ']';
                     $(input + '+label').attr('data-error',value)
                     $(input).addClass('invalid');
                });
                toastr["error"]('{{ trans('messages.error_registered')}}');
            });
        });

        });

    </script>

    @if(Session::has('info'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr["info"]('{{ Session::get('info') }}')
             });
        </script>
        {{Session::forget('info')}}
    @endif

    @if(Session::has('error'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr["error"]('{{ Session::get('error') }}')
             });
        </script>
        {{Session::forget('error')}}
    @endif

    @if(Session::has('warning'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr["warning"]('{{ trans('messages.noaccess') }}')
             });
        </script>
        {{Session::forget('error')}}
    @endif



    // If there is error, the modal will be trigger.
    @if(Session::get('method')=='login')
        <script>
        $(function() {
            $('#login-overlay').modal('show');
        });
        toastr["warning"]('{{ trans('messages.notloggedin')}}');
        </script>
    @endif

</body>

</html>
