  <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav fixed sn-bg-1 custom-scrollbar">
        <!-- Logo -->
        <li>
            <div class="logo-wrapper waves-light">
                <a href="{{ LaravelLocalization::getLocalizedURL(null,'dashboard') }}"><img src="{{asset('img/hooktothecloud.png')}}" class="img-fluid flex-center"></a>
            </div>
        </li>
        <!--/. Logo -->
        <!--Social-->
        <li>
            <ul class="social">
                <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a></li>
                <li><a class="icons-sm pin-ic"><i class="fa fa-pinterest"> </i></a></li>
                <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a></li>
                <li><a class="icons-sm tw-ic"><i class="fa fa-twitter"> </i></a></li>
            </ul>
        </li>
        <!--/Social-->
        <!--Search Form-->
        <li>
            <form class="search-form" role="search">
                <div class="form-group waves-light">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </form>
        </li>
        <!--/.Search Form-->
        <!-- Side navigation links -->
        <li>
            @if(Auth::user()->isAdminstrator())
                <li><a  href="{{ LaravelLocalization::getLocalizedURL(null,'administration') }}" class="collapsible collapsible-accordion"><i class="fa fa-suitcase"></i> @lang('menu.administration')</a></li>
            @endif
    
            <ul class="collapsible collapsible-accordion">
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-exchange"></i>@lang('menu.transfers')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="#" class="waves-effect">@lang('menu.transfers_receiving')</a>
                            </li>
                            <li><a href="#" class="waves-effect">@lang('menu.transfers_sending')</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-road"></i>@lang('menu.routes')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="#" class="waves-effect">@lang('menu.routes_receiving')</a>
                            </li>
                            <li><a href="#" class="waves-effect">@lang('menu.routes_sending')</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-cogs"></i>@lang('menu.account_settings')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{ LaravelLocalization::getLocalizedURL(null,'myenterprises') }}" class="waves-effect">@lang('menu.enterprises')</a>
                            </li>
                            <li><a href="#" class="waves-effect">@lang('menu.users')</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-eye"></i>@lang('menu.gettinghelp')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="#" class="waves-effect">@lang('menu.getting_help_tutorial')</a>
                            </li>
                            <li><a href="#" class="waves-effect">@lang('menu.getting_help')</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <!--/. Side navigation links -->
        <div class="sidenav-bg mask-strong"></div>
    </ul>
    <!--/. Sidebar navigation -->