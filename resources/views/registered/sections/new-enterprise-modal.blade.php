<!--Modal: New enterprise Form-->
<div class="modal fade" id="create-enterprise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header  mdb-color darken-4 white-text">
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="title"><i class="fa fa-user-plus"></i> @lang('enterprise.add.modaltitle')</h4>
            </div>

            <!--Body-->
            <form class="form-horizontal" role="form" id="formNewEnterprise" method="POST" action="{{ LaravelLocalization::getLocalizedURL(null,'createnterprise') }}">
              <div class="modal-body container-fluid">
                  {!! csrf_field() !!}
                  <p class="lead">@lang('enterprise.add.title')</p>

                   <!-- Enterprise name -->
                  <div class="row">
                    <div class="col-md-12">
                       <div class="md-form form-sm">
                          <i class="fa  fa-industry prefix darken-4"></i>
                        <input type="text"
                               name="enterprise_name"
                               id="enterprise_name"
                               class="form-control"
                               data-geo="name"
                               tabindex="1"
                               autofocus
                        >
                        <label for="name">@lang('enterprise.add.label.enterprise_name')</label>
                      </div>
                    </div>
                  </div>

                   <!-- VAT number and CBO -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="cbo"
                               id="cbo"
                               class="form-control validate"
                               tabindex="4">
                        <label for="company_ncboame">@lang('enterprise.add.label.cbo')</label>
                      </div>
                    </div>
                    <div class="col-sm-2">
                       <div class="md-form form-sm">
                            <input type="checkbox"
                               name="VATcheckbox"
                               id="VATcheckbox"
                               value="1"
                               tabindex="2">
                            <label for="VATcheckbox">@lang('enterprise.edit.VATpresent')</label>
                       </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="md-form form-sm">
                        <i class="fa fa-address-card-o darken-4 prefix"></i>
                        <input type="text"
                               name="vatnumber"
                               id="vatnumber"
                               class="form-control validate"
                               tabindex="3"
                               disabled="true"
                               >
                        <label for="vatnumber" data-toggle="tooltip" data-placement="top" title="@lang('enterprise.add.tooltip.vatnumber')">@lang('enterprise.add.label.vatnumber')</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                     <div class="map_canvas"></div>
                  </div>
                  <!-- address -->
                  <div class="row">
                    <div class="col-md-8">
                       <div class="md-form form-sm">
                          <i class="fa  fa-envelope prefix darken-4"></i>
                        <input type="text"
                               name="street"
                               id="street"
                               class="form-control"
                               data-geo="route"
                               tabindex="1"
                               autofocus
                        >
                        <label for="street">@lang('address.add.label.street')</label>
                      </div>
                    </div>
                    <div class="col-md-2">
                       <div class="md-form form-sm">
                          <i class="fa  fa-industry prefix darken-4"></i>
                        <input type="text"
                               name="number"
                               id="number"
                               data-geo="street_number"
                               class="form-control"
                               tabindex="1"
                               autofocus
                        >
                        <label for="name">@lang('address.add.label.streetnumber')</label>
                      </div>
                    </div>
                  </div>

                   <!--zip city -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="zip"
                               id="zip"
                               data-geo="postal_code"
                               class="form-control validate"
                               tabindex="4">
                        <label for="zip">@lang('address.add.label.zip')</label>
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="city"
                               id="city"
                               class="form-control validate"
                               data-geo="locality"
                               tabindex="4">
                        <label for="city">@lang('address.add.label.city')</label>
                      </div>
                    </div>
                  </div>

                  <!-- country -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="country"
                               id="country"
                               class="form-control validate"
                               data-geo="country_short"
                               tabindex="4">
                        <label for="zip">@lang('address.add.label.country')</label>
                      </div>
                    </div>
                  </div>

              </div>

              <!--Footer-->
              <div class="modal-footer">
                <button type="submit"
                        class="btn btn-primary waves-effect"
                        id="creating-button"
                        tabindex="5"
                        >
                  @lang('enterprise.add.button.create')
                </button>
              </div>
          </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: New enterprise Form-->