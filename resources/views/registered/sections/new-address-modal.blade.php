<!--Modal: New address Form-->
<div class="modal fade" id="create-address" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header  mdb-color darken-4 white-text">
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="title"><i class="fa fa-user-plus"></i> @lang('address.add.modaltitle')</h4>
            </div>

            <!--Body-->
            <form class="form-horizontal" role="form" id="formNewEnterprise" method="POST" action="{{ LaravelLocalization::getLocalizedURL(null,'createnterprise') }}">
              <div class="modal-body container-fluid">   
                  {!! csrf_field() !!}
                  <p class="lead">@lang('address.add.title')</p>
                  
                   <!-- address -->
                  <div class="row">
                    <div class="col-md-8">
                       <div class="md-form form-sm">
                          <i class="fa  fa-industry prefix darken-4"></i>
                        <input type="text"
                               name="street"
                               id="street"
                               class="form-control"
                               tabindex="1"
                               autofocus
                        >
                        <label for="name">@lang('address.add.label.street')</label>
                      </div>
                    </div>
                    <div class="col-md-2">
                       <div class="md-form form-sm">
                          <i class="fa  fa-industry prefix darken-4"></i>
                        <input type="text"
                               name="number"
                               id="number"
                               class="form-control"
                               tabindex="1"
                               autofocus
                        >
                        <label for="name">@lang('address.add.label.street')</label>
                      </div>
                    </div>
                  </div>
              
                   <!--zip city -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="zip"
                               id="zip"
                               class="form-control validate"
                               tabindex="4">
                        <label for="zip">@lang('address.add.label.zip')</label>
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="city"
                               id="city"
                               class="form-control validate"
                               tabindex="4">
                        <label for="zip">@lang('address.add.label.city')</label>
                      </div>
                    </div>
                  </div>

                  <!-- country -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="country"
                               id="country"
                               class="form-control validate"
                               tabindex="4">
                        <label for="zip">@lang('address.add.label.country')</label>
                      </div>
                    </div>
                  </div>
              </div>
            
              <!--Footer-->
              <div class="modal-footer">
                <button type="submit"
                        class="btn btn-primary waves-effect"
                        id="creating-button"
                        tabindex="5"
                        >
                  @lang('enterprise.add.button.create')
                </button>
              </div>
          </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: New enterprise Form-->