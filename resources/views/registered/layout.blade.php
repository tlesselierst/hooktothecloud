<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Application to make the link between classic sftp protocol and the recent REST ">
    <meta name="author" content="Tanguy Lesseliers">


    <title>Hook to the Cloud | @yield('pagetitle')</title>

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- Material Design Bootstrap -->
    <link rel="stylesheet"  type="text/css" href="{{ asset('css/mdb.min.css') }}">
    <!-- Your custom styles (optional) -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/httc.css') }}">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="fixed-sn grey-skin bg-skin-lp">

  <!-- Start your project here-->
    <header>
        <!-- Topmenu -->
        @include('registered.sections.sidenav')
        @include('registered.sections.navbar')
    </header>
    @yield('header')


    <main>
        @include('registered.sections.flash-message')
        <!-- page content -->
        @yield('main')
        <!-- end content -->
    </main>
    
    <footer>
        @yield('footer')
        <!--Copyright-->

    </footer>
    <!-- /Start your project here-->
    

    <!-- SCRIPTS -->

    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.1.1.min.js')}}"></script>

    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/tether.min.js')}}"></script>

    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js')}}"></script>

    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js')}}"></script>


    @if(Session::has('info'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr["info"]('{{ Session::get('info') }}')
             });
        </script>
        {{Session::forget('info')}}
    @endif

    @if(Session::has('error'))
        <script type="text/javascript">
            $(document).ready(function() {
                toastr["error"]('{{ Session::get('error') }}')
             });
        </script>
        {{Session::forget('error')}}
    @endif


    @yield('scripts')
</body>

</html>