@extends('registered.layout')

@section('main')
<div class="main-wrapper">
	<div class="container-fluid">
		<div class="row">
		<div class="clearfix">
		<h2 class="h2-responsive m-b-2">@lang('enterprises.title.addNewEnterprise')</h2>
		<a  href="#create-enterprise"  data-toggle="modal" data-target="#create-enterprise"  class="btn btn-primary btn-sm " role="button" rel="nofollow"><i class="fa fa-plus left"></i>@lang('enterprises.btn.addNewEnterprise')</a>
		</div>
		<!--/Crucial button-->
		</div>

		<!--Table-->
		<table class="table table-sm table-striped table-hover">
			<!--Table head-->
			<thead>
				<tr>
					<th>@lang('enterprises.header.enterprise_name')</th>
					<th>@lang('enterprises.header.vatnumber')</th>
					<th>@lang('enterprises.header.cbo')</th>
					<th>@lang('enterprises.header.invoice_address_id')</th>
					<th>@lang('enterprises.header.action')</th>
				</tr>
			</thead>
			<!--/Table head-->

			<!--Table body-->
			<tbody>
				@foreach($enterprises as $enterprise)
				<tr scope="row">
					<td> {{$enterprise->enterprise_name}} </td>
					<td> {{$enterprise->vatnumber}} </td>
					<td> {{$enterprise->cbo}} </td>
					<td> {{$enterprise->invoice_address_id}} </td>
					<td> </td>
				</tr>
				@endforeach
			</tbody>
		</table>
		<!--/Table-->
	</div>
	@include('registered.sections.new-enterprise-modal')
	@include('registered.sections.new-address-modal')
</div>
<!--Main Layout-->
 @endsection

@section('scripts')

@section('scripts')
<script type="text/javascript">

   //script to manage AJAX call to update and process feedback
    $(document).on('submit', '#formNewEnterprise', function(e) {
            e.preventDefault();

            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                $('.alert-success').removeClass('hidden');
                $('#create-enterprise').modal('hide');
                 toastr["info"]('{{ trans('messages.enterprise_created')}}');
                window.location = "{{ LaravelLocalization::getLocalizedURL(null,'/myenterprises') }}";

            })
            .fail(function(data) {
                $.each(data.responseJSON, function (key, value) {
                    var input = '#formNewEnterprise input[name=' + key + ']';
                     $(input + '+label').attr('data-error',value)
                     $(input).addClass('invalid');
                });
                toastr["error"]('{{ trans('messages.error_creating_enterprise')}}');
            })
         });
</script>

<script type="text/javascript">
     //script to  manage the change email checkbox
    $(function () {

        // Event Handlers
        $('#VATcheckbox').on('change', function () {
            var isChangeChecked =$('#VATcheckbox').is(':checked');

            if(isChangeChecked)  {
             $('#vatnumber').removeAttr('disabled');

            }
            else {
                $('#vatnumber').attr('disabled', 'disabled');
            }
        });
    });
  </script>

  <script type="text/javascript">

  	$('#formNewEnterprise').on('shown.bs.modal', function () {

	    setTimeout(function (){
	        $('#enterprise_name').focus();
	    }, 1000);

	});
  </script>

  <script type="text/javascript">
  	// Tooltips Initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

  </script>

  <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDeDvdz42QsBfCMaBY0LUbpDuaRYnQy3Bo&libraries=places&result_type=street_address"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.geocomplete.min.js')}}"></script>

   <script>
      $(function(){
        $("#street").geocomplete({
          details: "form",
          types: ["geocode", "establishment"],
          detailsAttribute: "data-geo",
        });
      });
    </script>
@endsection