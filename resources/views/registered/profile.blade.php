@extends('registered.layout')

@section('main')
<!--Main layout-->
<div class="main-wrapper">
	<div class="container-fluid">
		<div class="row">
		<!--Main column-->
		<div class="col-md-12">
			<div class="text-center">
				<h1 class="h2-responsive">Account details</h1>
			</div>
		</div>
    <br>
		<!--/.Main column-->

		</div>

		 <form class="form-horizontal" role="form" id="formEditUser-{{$user->id}}" method="POST" action="{{ LaravelLocalization::getLocalizedURL(null,'/updateProfile') }}">
      {!! csrf_field() !!}
      <input type="hidden" name="id" value="{{ $user->id }}">
    <!--First row-->
     <!-- First and last name -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                          <input type="text"
                               name="first_name"
                               id="first_name"
                               value="{{ $user->first_name}}"
                               class="form-control validate required"
                               tabindex="1"
                               autofocus="">
                          <label for="first_name">@lang('login.register.label.firstname')</label>
                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                        <input type="text"
                               name="last_name"
                               id="last_name"
                               value="{{ $user->last_name}}"
                               class="form-control validate required"
                               tabindex="2">
                        <label for="last_name">@lang('login.register.label.lastname')</label>
                      </div>
                    </div>
                  </div>

                  <!-- Display name -->
                  <div class="row">
                    <div class="col-md-12">
                       <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                        <input type="text"
                               name="name"
                               id="name"
                               value="{{ $user->name}}"
                               class="form-control"
                               tabindex="3">
                        <label for="name">@lang('login.register.label.displayname')</label>
                      </div>
                    </div>
                  </div>
              
                   <!-- Company and VAT number -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="company_name"
                               id="company_name"
                               value="{{ $user->company_name}}"
                               class="form-control validate"
                               tabindex="4">
                        <label for="company_name">@lang('login.register.label.company')</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                        <i class="fa fa-address-card-o darken-4 prefix"></i>
                        <input type="text"
                               name="vatnumber"
                               id="vatnumber"
                               value="{{ $user->vatnumber}}"
                               class="form-control validate"
                               tabindex="5">
                        <label for="vatnumber">@lang('login.register.label.vatnumber')</label>
                      </div>
                    </div>
                  </div>

                   <!-- email  -->
                  <div class="row">

                    <div class="col-md-6 col-sm-3 col-md-3">
                      <div class="md-form form-sm">
                        <input type="checkbox"
                               name="changeEmailCheckbox"
                               id="changeEmail"
                               value="1"
                               tabindex="7">
                        <label for="changeEmail">@lang('user.edit.changeEmail')</label>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="md-form form-sm">
                        <i class="fa fa-envelope  darken-4 prefix"></i>
                        <input type="email"
                               name="email"
                               id="email"
                               value="{{ $user->email}}"
                               class="form-control"
                               tabindex="6"
                               disabled="true">
                        <label for="email">@lang('login.register.label.email')</label>
                      </div>
                    </div>
                  </div>

                   <!-- password -->
                  <div class="row">
                    <p class="lead">@lang('user.leaveemptypassword')</p>
                  </div>
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-lock  darken-4 prefix"></i>
                          <input type="password"
                                 name="password"
                                id="password"
                                class="form-control"
                                tabindex="7">
                          <label for="password">@lang('login.register.label.password')</label>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                       <div class="md-form form-sm">
                          <i class="fa fa-lock  darken-4 prefix"></i>
                          <input type="password"
                                name="password_confirmation"
                                id="password_confirmation"
                                 class="form-control"
                                tabindex="8">
                          <label for="password_confirmation">@lang('login.register.label.confirmpwd')</label>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                  <div class="col-md-12">
                   <button type="submit"
                        class="btn btn-primary waves-effect"
                        id="registering-button"
                        tabindex="10"
                        >
                  @lang('user.edit.button.save')
                </button>
                </div>
                  </div>
</form>
         
	</div>
</div>
 @endsection

 @section('scripts')
<script type="text/javascript">
     //script to  manage the change email checkbox
    $(function () {
       
        // Event Handlers
        $('#changeEmail').on('change', function () {
            var isChangeChecked =$('#changeEmail').is(':checked'); 

            if(isChangeChecked)  {
             $('#email').removeAttr('disabled');

            }
            else {
                $('#email').attr('disabled', 'disabled');
            }
        });
    });
  </script>
  @endsection