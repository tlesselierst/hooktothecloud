@extends('admin.admin_template')

@section('main')
<div class="main-wrapper">
    <div class="container-fluid">
        <div class="row">
        <div class="clearfix">
        <h2 class="h2-responsive m-b-2">@lang('enterprises.title.addNewEnterprise')</h2>
        <a  href="#create-enterprise"  data-toggle="modal" data-target="#create-enterprise"  class="btn btn-primary btn-sm " role="button" rel="nofollow"><i class="fa fa-plus left"></i>@lang('enterprises.btn.addNewEnterprise')</a>
        </div>
        <!--/Crucial button-->
        </div>
    
        <!--Table-->
        <table class="table table-sm table-striped table-hover">
            <!--Table head-->
            <thead>
                <tr>
                    <th>@lang('enterprises.header.enterprise_name')</th>
                    <th>@lang('enterprises.header.vatnumber')</th>
                    <th>@lang('enterprises.header.cbo')</th>
                    <th>@lang('enterprises.header.invoice_address_id')</th>
                    <th>@lang('enterprises.header.action')</th>
                </tr>
            </thead>
            <!--/Table head-->

            <!--Table body-->
            <tbody>
                @foreach($enterprises as $enterprise)
                <tr scope="row">
                    <td> {{$enterprise->enterprise_name}} </td>
                    <td> {{$enterprise->vatnumber}} </td>
                    <td> {{$enterprise->cbo}} </td>
                    <td> {{$enterprise->invoice_address_id}} </td>
                    <td> </td>
                </tr>
                @endforeach
            </tbody>                  
        </table>
        <!--/Table-->
    </div>
   
</div>
<!--Main Layout-->
@endsection