@extends('admin.admin_template')

@section('main')
<!--Main layout-->

    <div class="container-fluid mt-5">
        <h2>You're now in the user reserved section for the Application Administrator</h2>
        <br>

		<div class="clearfix">
		 <a  href="#create-user"  data-toggle="modal" data-target="#create-user"  class="btn btn-primary btn-sm " role="button" rel="nofollow">@lang('user.btn.addnewuser')</a>
		</div>
         

		<table class="table table-sm table-striped table-hover">
			<thead>
				<tr>
				<th>@lang('user.header.firstname')</th>
				<th>@lang('user.header.lastname')</th>
				<th>@lang('user.header.name')</th>
				<th>@lang('user.header.email')</th>
				<th>@lang('user.header.company_name')</th>
				<th>@lang('user.header.action')</th>
			</tr>
			</thead>
			<tbody>
				 @foreach($users as $user)
		          <tr scope="row">
		              <td> {{$user->first_name}} </td>
		              <td> {{$user->last_name}} </td>
		              <td> {{$user->name}} </td>
		              <td> {{$user->email}} </td>
		              <td> {{$user->company_name}} </td>
		              <td>  <a  data-toggle="modal" data-target="#edit-user-{{$user->id}}"  class="btn btn-secondary btn-sm" role="button" rel="nofollow" id="modal-edit" title="Edit">@lang('user.btn.edit')</a> </td>
		          </tr>
		         @endforeach
			</tbody>
		</table>

    </div>
       @include('admin.sections.modal-user-new')
       @each('admin.sections.modal-user-edit',$users, 'user')
<!--Main Layout-->
@endsection
@section('scripts')
    <script type="text/javascript">
    //script to manage AJAX call to update and process feedback
    $(document).on('submit', '#formNewUser', function(e) {  
            e.preventDefault();
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                $('.alert-success').removeClass('hidden');
                $('#create-user').modal('hide');
                 toastr["info"]('{{ trans('messages.user_created')}}');
                window.location = "{{ LaravelLocalization::getLocalizedURL(null,'/users') }}";
                
            })
            .fail(function(data) {
                $.each(data.responseJSON, function (key, value) {
                    var input = '#formNewUser input[name=' + key + ']';
                     $(input + '+label').attr('data-error',value)
                     $(input).addClass('invalid');
                });
                toastr["error"]('{{ trans('messages.error_creating_user')}}');
            })
         });



    

    </script>        


       @each('admin.sections.modal-user-edit-js',$users, 'user')
@endsection