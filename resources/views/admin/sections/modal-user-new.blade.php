<!--Modal: New user Form-->
<div class="modal fade" id="create-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog cascading-modal modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">

            <!--Header-->
            <div class="modal-header  mdb-color darken-4 white-text">
                <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="title"><i class="fa fa-user-plus"></i> @lang('user.add.modaltitle')</h4>
            </div>

            <!--Body-->
            <form class="form-horizontal" role="form" id="formNewUser" method="POST" action="{{ LaravelLocalization::getLocalizedURL(null,'/createuser') }}">
              <div class="modal-body container-fluid">   
                  {!! csrf_field() !!}
                  <p class="lead">@lang('user.add.title')</p>
                  
                   <!-- First and last name -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                          <input type="text"
                               name="first_name"
                               id="first_name"
                               class="form-control validate required"
                               tabindex="1"
                               autofocus="">
                          <label for="first_name">@lang('user.add.label.firstname')</label>
                      </div>

                    </div>
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                        <input type="text"
                               name="last_name"
                               id="last_name"
                               class="form-control validate required"
                               tabindex="2">
                        <label for="last_name">@lang('user.add.label.lastname')</label>
                      </div>
                    </div>
                  </div>

                  <!-- Display name -->
                  <div class="row">
                    <div class="col-md-12">
                       <div class="md-form form-sm">
                          <i class="fa fa-user prefix darken-4"></i>
                        <input type="text"
                               name="name"
                               id="name"
                               class="form-control"
                               tabindex="3">
                        <label for="name">@lang('user.add.label.displayname')</label>
                      </div>
                    </div>
                  </div>
              
                   <!-- Company and VAT number -->
                  <div class="row">
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-building  darken-4 prefix"></i>
                        <input type="text"
                               name="company_name"
                               id="company_name"
                               class="form-control validate"
                               tabindex="4">
                        <label for="company_name">@lang('user.add.label.company')</label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="md-form form-sm">
                        <i class="fa fa-address-card-o darken-4 prefix"></i>
                        <input type="text"
                               name="vatnumber"
                               id="vatnumber"
                               class="form-control validate"
                               tabindex="5">
                        <label for="vatnumber">@lang('user.add.label.vatnumber')</label>
                      </div>
                    </div>
                  </div>

                   <!-- email  -->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="md-form form-sm">
                        <i class="fa fa-envelope  darken-4 prefix"></i>
                        <input type="email"
                               name="email"
                               id="email"
                               class="form-control"
                               tabindex="6">
                        <label for="email">@lang('user.add.label.email')</label>
                      </div>
                    </div>
                  </div>

                   <!-- password -->
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                      <div class="md-form form-sm">
                          <i class="fa fa-lock  darken-4 prefix"></i>
                          <input type="password"
                                 name="password"
                                id="password"
                                class="form-control"
                                tabindex="7">
                          <label for="password">@lang('user.add.label.password')</label>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                       <div class="md-form form-sm">
                          <i class="fa fa-lock  darken-4 prefix"></i>
                          <input type="password"
                                name="password_confirmation"
                                id="password_confirmation"
                                 class="form-control"
                                tabindex="8">
                          <label for="password_confirmation">@lang('user.add.label.confirmpwd')</label>
                      </div>
                    </div>
                  </div>

              </div>
            
              <!--Footer-->
              <div class="modal-footer">
                <button type="submit"
                        class="btn btn-primary waves-effect"
                        id="registering-button"
                        tabindex="10"
                        >
                  @lang('user.add.button.create')
                </button>
              </div>
          </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: New user Form-->