@if ($errors->any())

<div id="popupmodal" class="modal fade" role="dialog" aria-labelledby="Close" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-body">
			<div class="alert alert-danger alert-dismissible" id="error-alert">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">@lang('validation.message.title')</h4>
				@lang('validation.message.intro')<br><br>
				<ul>
					@foreach($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div> 

@endif
@if (Session::has('success'))
  <div class="alert alert-success alert-dismissible fade in" id="success-alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>
      <i class="fa fa-check-circle fa-lg fa-fw"></i>Success &nbsp;
    </strong>
    {{Session::get('success')}}
  </div>
@endif

