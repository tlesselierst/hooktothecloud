 <!-- Navbar -->
    <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav navbar-dark unique-color">
        <!-- SideNav slide-out button -->
        <div class="float-xs-left">
            <a href="{{ LaravelLocalization::getLocalizedURL(null,'administration') }}" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
        </div>
        <!-- Breadcrumb-->
        <div class="breadcrumb-dn mr-auto">
            <p>@lang('menu.application_name')</p>
        </div>
        <ul class="nav navbar-nav nav-flex-icons ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/translations" ><i class="fa fa-language"></i> <span class="hidden-sm-down">@lang('menu.translations')</p>
        </div></span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/routes"><i class="fa fa-map-o"></i> <span class="hidden-sm-down">@lang('menu.routes')</p>
        </div></span></a>
            </li>
            @if(Auth::check())
                <li class="nav-item dropdown btn-group">
                    <a href="#" class="nav-link dropdown-toggle" id="dropdownUser" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <i class="fa fa-user"></i> <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <a href="{{ LaravelLocalization::getLocalizedURL(null,'/profile') }}" class="dropdown-item">@lang('menu.profile')</a></li>
                        <a href="{{ LaravelLocalization::getLocalizedURL(null,'/logout') }}"   class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> @lang('menu.logout')</a>
                            <form id="logout-form" 
                            action="{{ LaravelLocalization::getLocalizedURL(null,'/logout') }}" 
                            method="POST" 
                            style="display: none;">
                            {{ csrf_field() }}
                            </form>
                    </ul>
                </li>

                <li class="nav-item">
                    
                </li>
            @endif

        </ul>
    </nav>
    <!-- /.Navbar -->