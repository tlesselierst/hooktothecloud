<script type="text/javascript">
    var user = {!! json_encode($user->toArray()) !!};
    $(document).on('submit', '#formEditUser-{{$user->id}}', function(e) {  
            e.preventDefault();
             
            $.ajax({
                method: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                dataType: "json"
            })
            .done(function(data) {
                $('.alert-success').removeClass('hidden');
                $('#formEditUser-{{$user->id}}').modal('hide');
                 toastr["info"]('{{ trans('messages.user_updated')}}');
                window.location = "{{ LaravelLocalization::getLocalizedURL(null,'/users') }}";
                
            })
            .fail(function(data) {
                //var whtswrong ='';
                $.each(data.responseJSON, function (key, value) {
                    var input = '#formEditUser-{{$user->id}} input[name=' + key + ']';
                     $(input + '+label').attr('data-error',value)
                     $(input).addClass('invalid');
                     //whtswrong += ' key:' + key + ' value: '   +value;
                });
                toastr["error"]('{{ trans('messages.error_updating_user')}}');
                //toastr["info"](whtswrong);
            })
         });

    //script to  manage the change email checkbox
    $(function () {
       
        // Event Handlers
        $('#changeEmail-{{$user->id}}').on('change', function () {
            var isChangeChecked =$('#changeEmail-{{$user->id}}').is(':checked'); 

            if(isChangeChecked)  {
             $('#email-{{$user->id}}').removeAttr('disabled');

            }
            else {
                $('#email-{{$user->id}}').attr('disabled', 'disabled');
            }
        });
    });

    </script>        