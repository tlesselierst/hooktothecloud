  <!-- Sidebar navigation -->
    <ul id="slide-out" class="side-nav fixed custom-scrollbar navbar-dark unique-color">
        <!-- Logo -->
        <li>
            <div class="logo-wrapper waves-light">
                <a href="{{ LaravelLocalization::getLocalizedURL(null,'administration') }}"><img src="{{asset('img/hooktothecloud.png')}}" class="img-fluid flex-center"></a>
            </div>
        </li>
        <!--/. Logo -->
        <!--Social-->
        <li>
            <ul class="social">
                <li><a class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a></li>
                <li><a class="icons-sm pin-ic"><i class="fa fa-pinterest"> </i></a></li>
                <li><a class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a></li>
                <li><a class="icons-sm tw-ic"><i class="fa fa-twitter"> </i></a></li>
            </ul>
        </li>
        <!--/Social-->
        <!--Search Form-->
        <li>
            <form class="search-form" role="search">
                <div class="form-group waves-light">
                    <input type="text" class="form-control" placeholder="Search">
                </div>
            </form>
        </li>
        <!--/.Search Form-->
        <!-- Side navigation links -->
        <li>
            <li><a  href="{{ LaravelLocalization::getLocalizedURL(null,'dashboard') }}"" class="collapsible collapsible-accordion"><i class="fa fa fa-tachometer"></i>@lang('menu.dashboard')</a></li>
            <ul class="collapsible collapsible-accordion">
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-users"></i>@lang('menu.accounts')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="{{ LaravelLocalization::getLocalizedURL(null,'enterprises') }}" class="waves-effect">@lang('menu.enterprises')</a>
                            </li>
                            <li><a href="{{ LaravelLocalization::getLocalizedURL(null,'users') }}" class="waves-effect">@lang('menu.users')</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-tasks"></i>@lang('menu.transfers')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="#" class="waves-effect">@lang('menu.routes_receiving')</a>
                            </li>
                            <li><a href="#" class="waves-effect">@lang('menu.routes_sending')</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-eye"></i>@lang('menu.gettinghelp')<i class="fa fa-angle-down rotate-icon"></i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="#" class="waves-effect">@lang('menu.getting_help_tutorial')</a>
                            </li>
                            <li><a href="#" class="waves-effect">@lang('menu.getting_help')</a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
        <!--/. Side navigation links -->
        <div class="sidenav-bg mask-strong"></div>
    </ul>
    <!--/. Sidebar navigation -->