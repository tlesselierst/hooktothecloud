# What it is 

Right now this application is just a laravel skeleton with some localisation added and a translation manager 


# What it will become

A web application allowing someone to link his (s)ftp server to a web based REST interface to be able to exhange files between the traditional ftp and the modern Rest call

# Main features

* Design
 * Responsive
 * Based on MDBootstrap

* Registration 
 * login and passwird 
 * social sites 

* Free and paid subscription 

* Public and private site 
 * public : commercial website 
 * multilingual (en, fr, nl)
 * private : dashboard and admin

* Configure a Route 
 *From ( ftp, sftp, rest )
 *To ( Rest, ftp, sftp)


# Steps

1. laravel new hooktothecloud
2. cd hooktothecloud
3. change .env to point to correct db
4. composer require mcamara/laravel-localization
5. execute instructions of https://github.com/mcamara/laravel-localization
 1. php artisan vendor:publish --provider="Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider"
 2. Modify config/app.php 
6. git init
7. git remote add origin ssh://git@bitbucket.org/tlesselierst/hooktothecloud.git
8. composer require caouecs/laravel-lang:~3.0  (https://github.com/caouecs/Laravel-lang)
9. copy folders of languages to ressources/lang
10. composer require barryvdh/laravel-translation-manager
11. php artisan vendor:publish --provider="Barryvdh\TranslationManager\ManagerServiceProvider" --tag=migrations
12. execute data migrations on homestead
13. php artisan vendor:publish --provider ="Barryvdh\TranslationManager\ManagerServiceProvider" --tag=config
14. php artisan vendor:publish 11--provider="Barryvdh\TranslationManager\ManagerServiceProvider" --tag=views
15. npm install
16. npm install font-awesome

# Get it locally 
1. check you have access with ssh to bitbucket - ssh -T git@bitbucket.org
2. git clone git@bitbucket.org:tlesselierst/hooktothecloud.git hooktothecloud
3. cd hooktothecloud
4. composer update
5. [add site to homestead/ repprovision homestead]
 1. homestead ssh
 2. serve site hooktohecloud.app ~/Code/hooktothecloud/public
6. Configure the homestead database
 1. you're still in ssh of homstead
 2. mysql --user=homestead --password=secret
 3. create database hook;
 4. exit
7. Create and config .env file
 1. cp .env.example .env
 2. change variable DB_DATABASE=hook 
8. php artisan key:generate

 
